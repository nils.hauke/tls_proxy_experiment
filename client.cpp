#include <arpa/inet.h>
#include <cstring>
#include <iostream>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

int main() {
  // initialize SSL library
  SSL_library_init();

  // create a new SSL context
  SSL_CTX *ssl_context = SSL_CTX_new(TLS_client_method());

  // load the client certificate and key files
  SSL_CTX_use_certificate_file(ssl_context, "client_cert.pem",
                               SSL_FILETYPE_PEM);
  SSL_CTX_use_PrivateKey_file(ssl_context, "client_key.pem", SSL_FILETYPE_PEM);

  // create a new TCP socket for the client to connect on
  int client_socket = socket(AF_INET, SOCK_STREAM, 0);

  // configure the server address and port
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr =
      inet_addr("127.0.0.1"); // replace with actual server IP address
  server_address.sin_port = htons(8080); // replace with actual server port

  // connect to the server
  connect(client_socket, (struct sockaddr *)&server_address,
          sizeof(server_address));

  // create a new SSL connection and attach it to the socket
  SSL *ssl_connection = SSL_new(ssl_context);
  SSL_set_fd(ssl_connection, client_socket);

  // initiate the SSL handshake with the server
  SSL_connect(ssl_connection);

  // send a query string to the server
  std::string query_string = "Hello, server!";
  SSL_write(ssl_connection, query_string.c_str(), query_string.length());

  // read the response from the server
  char response_buffer[1024];
  int response_length = SSL_read(ssl_connection, response_buffer, 1024);

  // print the response
  std::string response_message(response_buffer, response_length);
  std::cout << "Response: " << response_message << std::endl;

  // close the SSL connection and socket
  SSL_shutdown(ssl_connection);
  SSL_free(ssl_connection);
  close(client_socket);

  // cleanup SSL context
  SSL_CTX_free(ssl_context);

  return 0;
}
