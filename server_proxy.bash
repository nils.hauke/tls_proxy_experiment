#!/bin/bash

socat OPENSSL-LISTEN:8080,cert=server_cert.pem,key=server_key.pem,verify=1,cafile=client_cert.pem,fork TCP4:127.0.0.1:${1:-8081}
