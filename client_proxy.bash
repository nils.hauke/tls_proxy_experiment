#!/bin/bash

socat TCP-LISTEN:8082,fork OPENSSL:127.0.0.1:8080,verify=1,cafile=server_cert.pem,cert=client_cert.pem,key=client_key.pem
