#include <cstring>
#include <iostream>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

int main() {
  // create a socket for the server to listen on
  int server_socket = socket(AF_INET, SOCK_STREAM, 0);

  // configure the server socket to allow reuse of the port
  int reuse = 1;
  setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

  // create a sockaddr_in struct to hold the address and port information
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = INADDR_ANY;
  server_address.sin_port = htons(8081); // listen on port 8081

  // bind the server socket to the address and port
  bind(server_socket, (struct sockaddr *)&server_address,
       sizeof(server_address));

  // listen for incoming connections
  listen(server_socket, 5); // allow up to 5 pending connections

  while (true) {
    // accept a new client connection
    int client_socket = accept(server_socket, NULL, NULL);

    // read the query string from the client
    char query_buffer[1024];
    read(client_socket, query_buffer, 1024);

    // create a response message
    std::string response_message = "Hello, world!\n";

    // send the response message to the client
    send(client_socket, response_message.c_str(), response_message.length(), 0);

    // close the client socket
    close(client_socket);
  }

  // close the server socket
  close(server_socket);

  return 0;
}
