#!/usr/bin/env python

import argparse
import time
from pylibmodbus import ModbusTcp

# Define command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-a", "--address", help="Modbus server address", default="127.0.0.1")
parser.add_argument("-p", "--port", help="Modbus server port", type=int, default=8082)
args = parser.parse_args()

# Create ModbusTcp client with command line arguments
client = ModbusTcp(args.address, port=args.port)
client.set_slave(1)

# Use the ModbusTcp client
# ...

# Connect to the Modbus server
client.connect()

# # Read the value of register 0x102
# client.write_register(0x102, 89)

# Read the value of register 0x102
result = client.read_registers(0x102, 1)

# Print the value of the register
print(f"Register 0x102: {result[0]}")
