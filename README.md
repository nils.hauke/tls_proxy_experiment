# Wrapping a Modbus TCP server in TLS

The basic ingredients are a server that uses TCP for communicating that uses a port that the client isn't aware of.
Then, a proxy forms the publically available end-point.

## Hello World

A simple example is given as a server that returns `Hello, world!`.
I wrote it to make sure that the proxy works.

### Preparation
Compile using

    mkdir build
    cd build
    cmake ..
    make

### Execution

Run the server with

    build/server

The server will listen to the port 8081.

Run the proxy with

    ./server_proxy.bash

The proxy will accept requests using TLS on port 8080 and forward them using TCP on port 8081.

Run the client with

    build/client

It will contact the server (actually, the proxy) on port 8080 and print `Response: Hello, world!`.
Ceritficate checking is enabled.

An alternative implementation of the proxy in C++ is available using

    build/proxy -c

The `-c` is optional and enables certificate checking. 

## mbwallbox

This experiment uses mbwallbox directly.

### Preparation

In `wallbox_sw`, check out the commit `3b86130e1e`. `develop` may also work. Apply the patch `prep_libwallbox_mb.patch`. It allows you to run `mbwallbox` without the rest of the stack and provides some useful output.

Build Quasar with

    ./makewbx -p quasar -m RPI -c r -j6

In the directory of this repo, prepare the virtual environment:

    pipenv install

### Execution

From `wallbox_sw` run

    ./build_quasar_rpi_release/libwallbox_mb/src/mbwallbox/mbwallbox

One of the lines printed are `Listening on port 8081`.
This is the port that I hard-coded. In production the port is going to come from the database.

Use the port number to run

    ./server_proxy.bash

This is the server proxy.
Provided you did the preparation step from the Hello, World! example, you can also use

    build/proxy -c

Run

    ./client_proxy.bash

This runs a proxy to wrap a TCP connection from the client in TLS (yes, I know how absurd this is).

Run the client in the virutal environment:

    pipenv shell
    ./client.py
    
In this setup, the port 8082 is used as the TCP port in the client proxy, 8081 is the TCP port in the server and 8080 is the TLS port used in the server proxy.
The output in the client should be `Register 0x102: 43`.

## Troubleshooting

The certificates provided have the CN `127.0.0.1`. If you run the examples in a different environment, things might go wrong.
