#include <arpa/inet.h>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <sys/socket.h>
#include <unistd.h>

const int CLIENT_PORT = 8080;

// Handle SSL errors
void handle_error(const char *msg) {
  std::cerr << "Error: " << msg << std::endl;
  ERR_print_errors_fp(stderr);
  exit(EXIT_FAILURE);
}

// Verify the client's certificate
int verify_callback(int preverify_ok, X509_STORE_CTX *ctx) {
  char buf[256];
  X509 *cert = X509_STORE_CTX_get_current_cert(ctx);
  X509_NAME_oneline(X509_get_subject_name(cert), buf, sizeof(buf));
  std::cout << "Subject: " << buf << std::endl;
  return preverify_ok;
}

void init_openssl() {
  SSL_load_error_strings();
  OpenSSL_add_ssl_algorithms();
}

void cleanup_openssl() { EVP_cleanup(); }

SSL_CTX *create_context() {
  SSL_CTX *ctx = SSL_CTX_new(TLS_server_method());

  if (!ctx) {
    ERR_print_errors_fp(stderr);
    exit(EXIT_FAILURE);
  }

  return ctx;
}

void configure_context(SSL_CTX *ctx, bool check_certificate) {
  // Set your own certificates and keys
  if (SSL_CTX_use_certificate_file(ctx, "server_cert.pem", SSL_FILETYPE_PEM) <=
      0) {
    ERR_print_errors_fp(stderr);
    exit(EXIT_FAILURE);
  }

  if (SSL_CTX_use_PrivateKey_file(ctx, "server_key.pem", SSL_FILETYPE_PEM) <=
      0) {
    ERR_print_errors_fp(stderr);
    exit(EXIT_FAILURE);
  }

  if (check_certificate) {
    // Load client's trusted root CA certificate
    if (SSL_CTX_load_verify_locations(ctx, "client_cert.pem", NULL) != 1) {
      handle_error("Failed to load client's trusted root CA certificate");
    }

    // Set up SSL/TLS verification options
    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                       verify_callback);
  }
}

int create_socket(int port) {
  int sockfd;
  struct sockaddr_in addr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("Unable to create socket");
    exit(EXIT_FAILURE);
  }

  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    perror("Unable to bind");
    exit(EXIT_FAILURE);
  }

  if (listen(sockfd, 1) < 0) {
    perror("Unable to listen");
    exit(EXIT_FAILURE);
  }

  return sockfd;
}

void proxy(SSL *ssl, int server_sock) {
  char buf[4096];
  ssize_t bytes_read;

  while (true) {
    // Read data from the client
    bytes_read = SSL_read(ssl, buf, sizeof(buf));
    if (bytes_read <= 0)
      break;

    // Write data to the server
    if (write(server_sock, buf, bytes_read) != bytes_read)
      break;

    // Read data from the server
    bytes_read = read(server_sock, buf, sizeof(buf));
    if (bytes_read <= 0)
      break;

    // Write data to the client
    if (SSL_write(ssl, buf, bytes_read) != bytes_read)
      break;
  }
}

int main(int argc, char *argv[]) {
  // Default values for command line options
  std::string address = "127.0.0.1";
  int port = 8080;
  bool check_certificate = false;

  // Parse command line options
  int opt;
  while ((opt = getopt(argc, argv, "a:p:c")) != -1) {
    switch (opt) {
    case 'a':
      address = optarg;
      break;
    case 'p':
      port = atoi(optarg);
      break;
    case 'c':
      check_certificate = true;
      break;
    default:
      std::cerr << "Usage: " << argv[0] << " [-a address] [-p port] [-c]"
                << std::endl;
      exit(EXIT_FAILURE);
    }
  }

  int client_sock, server_sock;
  SSL_CTX *ctx;
  SSL *ssl;

  init_openssl();
  ctx = create_context();
  configure_context(ctx, check_certificate);

  client_sock = create_socket(CLIENT_PORT);

  while (true) {
    struct sockaddr_in addr;
    uint len = sizeof(addr);
    int client;

    client = accept(client_sock, (struct sockaddr *)&addr, &len);
    if (client < 0) {
      perror("Unable to accept");
      continue;
    }

    ssl = SSL_new(ctx);
    SSL_set_fd(ssl, client);

    if (SSL_accept(ssl) <= 0) {
      ERR_print_errors_fp(stderr);
      SSL_shutdown(ssl);
      SSL_free(ssl);
      close(client);
      continue;
    }

    // Connect to the server
    server_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock < 0) {
      perror("Unable to create server socket");
      SSL_shutdown(ssl);
      SSL_free(ssl);
      close(client);
      continue;
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr.s_addr = inet_addr(address.c_str());

    if (connect(server_sock, (struct sockaddr *)&server_addr,
                sizeof(server_addr)) < 0) {
      perror("Unable to connect to server");
      SSL_shutdown(ssl);
      SSL_free(ssl);
      close(client);
      close(server_sock);
      continue;
    }

    // Start proxying data between the client and the server
    proxy(ssl, server_sock);

    // Clean up
    SSL_shutdown(ssl);
    SSL_free(ssl);
    close(client);
    close(server_sock);
  }

  close(client_sock);
  SSL_CTX_free(ctx);
  cleanup_openssl();
  return 0;
}
